package imagem;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class Imagem {
//Atributos
    private String numeroMagico;
    private String comentario;
    private int altura;
    private int largura;
    private int numeroMaximoCor;
    private int tamanhoCabecalho;
    private String enderecoArquivo;
    private byte[] pixels;
    private BufferedImage imagem;
    private FileInputStream arquivo;

//Construtor padrão
    public Imagem () {
        numeroMagico = "";
        comentario = "";
        altura = 0;
        largura = 0;
        numeroMaximoCor = 0;
        tamanhoCabecalho = 0;
        enderecoArquivo = "";
        pixels = null;
        arquivo = null;
    }
    
//Getters and Setters
    public String getNumeroMagico() {
        return numeroMagico;
    }

    public void setNumeroMagico(String numeroMagico) {
        this.numeroMagico = numeroMagico;
    }
    
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getNumeroMaximoCor() {
        return numeroMaximoCor;
    }

    public void setNumeroMaximoCor(int numeroMaximoCor) {
        this.numeroMaximoCor = numeroMaximoCor;
    }
    
    public int getTamanhoCabecalho() {
        return tamanhoCabecalho;
    }

    public void setTamanhoCabecalho(int tamanhoCabecalho) {
        this.tamanhoCabecalho = tamanhoCabecalho;
    }

    public String getEnderecoArquivo() {
        return enderecoArquivo;
    }

    public void setEnderecoArquivo(String enderecoArquivo) {
        this.enderecoArquivo = enderecoArquivo;
    }
    
    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }
    
    public byte[] getPixels() {
        return pixels;
    }
    
    public FileInputStream getArquivo() {
        return arquivo;
    }
    
    public void setArquivo(FileInputStream arquivo) {
        this.arquivo = arquivo;
    }
    
    
    //Métodos que sofrerão Override
    public abstract BufferedImage pegaPixels() throws IOException;
    public abstract BufferedImage filtroNegativo() throws FileNotFoundException, IOException;
    
    
    //Abre e fechar o arquivo
    public void abrirArquivo() throws FileNotFoundException{
        FileInputStream file = new FileInputStream(getEnderecoArquivo());
        setArquivo(file);
    }

    public void fecharArquivo() throws IOException {
        getArquivo().close();
    }
    
    
//Métodos para extrair dados do cabeçalho da imagem
    public String lerLinhasCabecalho () {
        String linha = "";
        byte caracter; 
        FileInputStream file = getArquivo();
        int cont = 0;
                
        try {
        while ((caracter = (byte) file.read()) != '\n') {
            linha += (char) caracter;
            }

        //System.out.println("Contador = " + cont);
        //System.out.println("Linha = " + linha);
        
        }
        catch (IOException e) {
        }
        
        return linha;
    }
    
    public void pegaDados (String linha) {
        //String linha;
        String linhaCabecalho = "";
        String verificador;                 //numero magico
        String cabecalhoConcatenado = "";       //Tamanho do comentario
        int alturaLocal = 0;
        int larguraLocal = 0;
        int numeroMaximoCores = 0;
        int numeroCaracteresCabecalho = 0;
           
        //linha = lerLinhasCabecalho();                    //numero magico
        verificador = linha; 
                                 
        if("P5".equals(linha) || "P6".equals(linha)) {
            linha = lerLinhasCabecalho();               //Comentario
            linhaCabecalho += linha; 


        Scanner in = new Scanner(linha);
  
	while (linha.startsWith("#")) {         
            linha = lerLinhasCabecalho();               //Outros comentarios
	}
        in.close();

                                
	in = new Scanner(linha);
	if(in.hasNext() && in.hasNextInt())
            larguraLocal = in.nextInt();
	else
            System.out.println("Arquivo corrompido");
	if(in.hasNext() && in.hasNextInt())
            alturaLocal = in.nextInt();
	else
	System.out.println("Arquivo corrompido");
                            
            linha = lerLinhasCabecalho();
            in.close();
            in = new Scanner(linha);
            numeroMaximoCores = in.nextInt();
            in.close();
            
            cabecalhoConcatenado = verificador + ' ' + linhaCabecalho + ' ' + alturaLocal + ' ' + larguraLocal + ' ' + numeroMaximoCores + '*';
            numeroCaracteresCabecalho = cabecalhoConcatenado.length();
        } 
        else 
                System.out.println("Extensão do arquivo inválida!");
        
        
        setNumeroMagico(verificador);
        setComentario(linhaCabecalho);
        setAltura(alturaLocal);
        setLargura(larguraLocal);
        setNumeroMaximoCor(numeroMaximoCores);
        setTamanhoCabecalho(numeroCaracteresCabecalho);
        
        /*
          System.out.println("Variaveis = " + verificador);
          System.out.println("Variaveis = " + linhaCabecalho);
          System.out.println("Variaveis = " + alturaLocal);
          System.out.println("Variaveis = " + larguraLocal);
          System.out.println("Variaveis = " + numeroMaximoCores);
          System.out.println(cabecalhoConcatenado);
          System.out.println(numeroCaracteresCabecalho);
        */
        }
        
    //Método para verificar se o número mágico corresponde a extensão do arquivo
       public static int verificaImagem (String enderecoArquivo) throws FileNotFoundException, IOException {
        int contador = 0;
        int caracter;
        String numeroMagicoVerifica = "";
        FileInputStream arquivo = new FileInputStream(enderecoArquivo);
                
        while(contador != 1){
            contador++;
            caracter = arquivo.read();
            if ((char)caracter == '#'){
                contador --;
                while((char)caracter != '\n') {
                    caracter = arquivo.read();
                }
            } else {
                while((char)caracter != '\n') {
                    numeroMagicoVerifica += (char)caracter;
                    caracter = arquivo.read();
                }
            }
        }
        arquivo.close();
            
        String extensao = enderecoArquivo.substring(enderecoArquivo.length()-3, enderecoArquivo.length());
        if (numeroMagicoVerifica.equals("P5") && extensao.equals("pgm")){
            return 1; 
            
        } else if (numeroMagicoVerifica.equals("P6") && extensao.equals("ppm")) {
            return 2; 
            
        } else {
            return 3; 
            
        }
           
       }
    
    /*
    public static void main(String[] args) {
        Imagem im = new Imagem();
        
        im.abrirArquivo();
        im.pegaDados();
       
    }
    */

    }

