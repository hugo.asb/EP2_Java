package imagem;



import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileNotFoundException;
import java.io.IOException;




public class ImagemPpm extends Imagem{
    //Atrbutos
    
    //construtor
    public ImagemPpm(String endereco) {
    setNumeroMagico("");
    setComentario("");
    setAltura(0);
    setLargura(0);
    setNumeroMaximoCor(0);
    setTamanhoCabecalho(0);
    setEnderecoArquivo(endereco);
    setPixels(null);
    setArquivo(null);
}

    @Override
    public BufferedImage pegaPixels() throws IOException {
        BufferedImage imagemPpm = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) imagemPpm.getRaster().getDataBuffer()).getData();
        int i = 0;
        int caracter = getArquivo().read();
        
        while (caracter != -1) {
            if(i == 3*getAltura()*getLargura() && (char) caracter == '\n') {
                
            } else {
                pixelsLocais[i+2] = (byte) caracter;
                i++;
                caracter = getArquivo().read();
                pixelsLocais[i] = (byte) caracter;
                i++;
                caracter = getArquivo().read();
                pixelsLocais[i-2] = (byte) caracter;
                i++;
            }
                caracter = getArquivo().read();
        }
        
        setPixels(pixelsLocais);
        
            /*
            Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(imagemPpm)));
            frame.pack();
            frame.setVisible(true);
            */
        return imagemPpm;
    
    }
    
    @Override
    public BufferedImage filtroNegativo() throws FileNotFoundException, IOException {
        BufferedImage filtroNegativoPpm = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroNegativoPpm.getRaster().getDataBuffer()).getData();
        
        for (int i = 0; i < 3*getLargura()*getAltura(); i++) {
            pixelsLocais[i] = (byte) (getNumeroMaximoCor() - getPixels()[i]);  
        }
        
        return filtroNegativoPpm;
        
        /*
            Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(filtroNegativoPpm)));
            frame.pack();
            frame.setVisible(true); 
        */
         }
    
    
    //Filtros RGB
    //Vermelho
    public BufferedImage filtroRed () throws FileNotFoundException, IOException {
        BufferedImage filtroRed = new BufferedImage (getLargura(), getAltura (), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroRed.getRaster().getDataBuffer()).getData();
        int i, zeraPixel = 0;
        
        for (i = 0; i < 3*getAltura()*getLargura(); i++) {
            zeraPixel++;
            
            switch (zeraPixel) {
                case 1:
                    pixelsLocais[i] = (byte) 0;
                    break;
                case 2:
                    pixelsLocais[i] = (byte) 0;
                    break;
                case 3:
                    pixelsLocais[i] = getPixels()[i];
                    zeraPixel = 0;
                    break;
            }
        }
        
        return filtroRed;
    }
    
    //Verde
    public BufferedImage filtroGreen () throws FileNotFoundException, IOException {
        BufferedImage filtroGreen = new BufferedImage (getLargura(), getAltura (), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroGreen.getRaster().getDataBuffer()).getData();
        int i, zeraPixel = 0;
        
        for (i = 0; i < 3*getAltura()*getLargura(); i++) {
            zeraPixel++;
            
            switch (zeraPixel) {
                case 1:
                    pixelsLocais[i] = (byte) 0;
                    break;
                case 2:
                    pixelsLocais[i] = getPixels()[i];
                    break;
                case 3:
                    pixelsLocais[i] = (byte) 0;
                    zeraPixel = 0;
                    break;
            }
        }
        
        
        return filtroGreen;
    }
    
    //Azul
    public BufferedImage filtroBlue () throws FileNotFoundException, IOException {
        BufferedImage filtroBlue = new BufferedImage (getLargura(), getAltura (), BufferedImage.TYPE_3BYTE_BGR);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroBlue.getRaster().getDataBuffer()).getData();
        int i, zeraPixel = 0;
        
        for (i = 0; i < 3*getAltura()*getLargura(); i++) {
            zeraPixel++;
            
            switch (zeraPixel) {
                case 1:
                    pixelsLocais[i] = getPixels()[i];                    
                    break;
                case 2:
                    pixelsLocais[i] = (byte) 0;
                    break;
                case 3:
                    pixelsLocais[i] = (byte) 0;
                    zeraPixel = 0;
                    break;
            }
        }
        
        return filtroBlue;
        
            /*
            Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(filtroBlue)));
            frame.pack();
            frame.setVisible(true);
            */
    }
    
    
   /*
    public static void main(String[] args) throws IOException {
        ImagemPpm pic = new ImagemPpm();
        
        pic.abrirArquivo();
        pic.pegaDados();
        pic.pegaPixels(); 
        //pic.filtroNegativo();
        //pic.filtroBlue();
        pic.fecharArquivo();
        
    }
    */
    
}
