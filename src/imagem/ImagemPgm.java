package imagem;


import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileNotFoundException;
import java.io.IOException;



public class ImagemPgm extends Imagem {
//Atributo
    private int posicaoInicialMensagem;
    
    
// Construtor
public ImagemPgm() {
    super();
    posicaoInicialMensagem = 0;
}

public ImagemPgm(String endereco) {
    setNumeroMagico("");
    setComentario("");
    setAltura(0);
    setLargura(0);
    setNumeroMaximoCor(0);
    setTamanhoCabecalho(0);
    setEnderecoArquivo(endereco);
    setPixels(null);
    setArquivo(null);
    posicaoInicialMensagem = 0;
}


//Get and Set
    public int getPosicaoInicialMensagem() {
        return posicaoInicialMensagem;
    }

    public void setPosicaoInicialMensagem(int posicaoInicialMensagem) {
        this.posicaoInicialMensagem = posicaoInicialMensagem;
    }
    
    
//Metodo para pegar posição inicial da mensagem
    public void pegaInicioMensagem () {
        String linhaComInicio = getComentario();
        int i = 0;
        String inicioString = "";
        int inicioInt;
        char aux[];
        aux = linhaComInicio.toCharArray();
        
        while (aux[i] != '0' && aux[i] != '1' && aux[i] != '2' &&  aux[i] != '3' && aux[i] != '4' && aux[i] != '5' && aux[i] != '6' &&
               aux[i] != '7' && aux[i] != '8' && aux[i] != '9') {
                i++;
        }
            while (aux[i] == '0' || aux[i] == '1' || aux[i] == '2' ||  aux[i] == '3' || aux[i] == '4' || aux[i] == '5' || aux[i] == '6' ||
                   aux[i] == '7' || aux[i] == '8' || aux[i] == '9') { 
                inicioString += aux[i];
                i++;
            }
            inicioInt = Integer.parseInt(inicioString);
            setPosicaoInicialMensagem(inicioInt);
                        //System.out.println(inicioString + inicioInt);

    }

//Métodos que sofrem Override
    @Override
    public BufferedImage pegaPixels() throws IOException {
        BufferedImage imagemPgm = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) imagemPgm.getRaster().getDataBuffer()).getData();
        int i = 0;
        int caracter = getArquivo().read();
        while(caracter != -1) {
            pixelsLocais[i] = (byte)caracter;
            i++;
            caracter = getArquivo().read(); 
        }
        
        setPixels(pixelsLocais);
        
        /*Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(imagemPgm)));
            frame.pack();
            frame.setVisible(true); */

        return imagemPgm;
    }

    @Override
    public BufferedImage filtroNegativo() throws FileNotFoundException, IOException {
        BufferedImage filtroNegativoPgm = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroNegativoPgm.getRaster().getDataBuffer()).getData();
        
        for (int i = 0; i < getLargura()*getAltura(); i++) {
            pixelsLocais[i] = (byte) (getNumeroMaximoCor() - getPixels()[i]);  
        }
        
        /*
            Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(filtroNegativoPgm)));
            frame.pack();
            frame.setVisible(true); */
        
        return filtroNegativoPgm;
    }
    
    
//Decifrar a imagem
    public String decifraImagem () {
        int inicio = getPosicaoInicialMensagem();   
	int controle = 0;           
	int repeticoes = 0;       
	int conta_hash = 0;       
	char b = 0; 	//Bytes onde serao realizadas as operacoes de bitwise
	char b2;
        String mensagem = "";

	for(int i = inicio; i <= getPixels().length; i++) 	// Percorre os bytes da imagem
	{
		controle++;
		
		if (controle == 1)
		{
			b = (char) getPixels()[i];   
			b = (char) (b & 0x01);        //Operacao 'and' binaria para bit menos significativo
			b = (char) (b << 1);          //Shift para mover o bit uma casa para a esquerda
		repeticoes++;
		}

		else if (controle == 2) 
		{
			b2 = (char) getPixels()[i];  
			b2 = (char) (b2 & 0x01);      
			b = (char) (b | b2);          //Operacao 'or' binaria
		repeticoes++;

			if (repeticoes < 8) 
			{
				b = (char) (b << 1);    //Primeira repeticao: 00000xx0  
				controle = 1;   
			} 

			else {

				if (conta_hash == 0) 
				{
					conta_hash = 1;  

					//System.out.print(b); 
                                        mensagem += b;
					controle = 0;       
					repeticoes = 0;   
				} 

				else 
				{
					if (b == '#') 
					{                                          
					//System.out.println("\n" + "Mensagem esteganografada com sucesso"); 
					break;
					} 

					else 
					{

				
						//System.out.print(b);    
                                            mensagem += b;
						controle = 0;        
						repeticoes = 0;    
					}
				}
			}
		}
	}
        //System.out.println(mensagem);
        return mensagem;
    }
    
    
    //Filtro Sharpen 
    public BufferedImage filtroSharpen () throws IOException {
        BufferedImage filtroSharpen = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroSharpen.getRaster().getDataBuffer()).getData();
        char [] pixelsAux = new char[getAltura() * getLargura()];
        int caracter = 0, aux = 0, valorPixel = 0;
        int coeficiente[] = {0,-1,0, -1,5,-1, 0,-1,0};
        int teste = 0;
        abrirArquivo();

        //Pula os caracteres de comentário
        while (aux < getTamanhoCabecalho()) {
            caracter = getArquivo().read();
            aux++;   
        }
        aux = 0;
        do {
            caracter = getArquivo().read();
            pixelsAux[aux] = (char) caracter;
            aux++;
        }
        while (aux < getAltura()*getLargura());
        fecharArquivo();
        
        //Pegar os pixels que não serão alterados (bordas)
        for(int countA = 0; countA < 3; countA++){
		for(int countB = 0; countB < getAltura(); countB++){
                        pixelsLocais[countA + countB*getAltura()] = (byte)getPixels()[countA + countB*getAltura()];
		}
	}
	for(int countA = 0; countA < 3; countA++){
		for(int countB = 0; countB < getLargura(); countB++){
                        pixelsLocais[countA + countB*getAltura()] = (byte)getPixels()[countA + countB*getAltura()];
		}
	}
        
        //Modifica os pixels que têm todos os "vizinhos"
	for(int countA = 3/2; countA < getAltura()-3/2; countA++){
		for(int countB = 3/2; countB < getLargura()-3/2; countB++){
                    valorPixel = 0;
			for(int countC = -3/2; countC <= 3/2; countC++){
				for(int countD = -3/2; countD <= 3/2; countD++){
					valorPixel += coeficiente[(countC + 1) + 3*(countD + 1)] * 
                                        (pixelsAux[(countA + countC) + (countD + countB) * getAltura()]);
				}
			}
				//Tratando caso o pixel seja menor ou ultrapasse o número de cores
				valorPixel /= 1;
				valorPixel = (valorPixel < 0) ? 0 : valorPixel;
				valorPixel = (valorPixel > getNumeroMaximoCor()) ? getNumeroMaximoCor() : valorPixel;
                                pixelsLocais[countA + countB*getAltura()] = (byte) valorPixel;
		}
	}        

            /*
            Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(filtroSharpen)));
            frame.pack();
            frame.setVisible(true);*/
        
        return filtroSharpen;
        
    }
    
    //Filtro Blur 
    public BufferedImage filtroBlur () throws IOException {
        BufferedImage filtroBlur = new BufferedImage(getLargura(), getAltura(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixelsLocais;
        pixelsLocais = ((DataBufferByte) filtroBlur.getRaster().getDataBuffer()).getData();
        char [] pixelsAux = new char[getAltura() * getLargura()];
        int caracter = 0, aux = 0, valorPixel = 0;
        int coeficiente[] = {1,1,1, 1,1,1, 1,1,1};
        int teste = 0;
        abrirArquivo();

        //Pula os caracteres de comentário
        while (aux < getTamanhoCabecalho()) {
            caracter = getArquivo().read();
            aux++;   
        }
        aux = 0;
        do {
            caracter = getArquivo().read();
            pixelsAux[aux] = (char) caracter;
            aux++;
        }
        while (aux < getAltura()*getLargura());
        fecharArquivo();
        
        //Pegar os pixels que não serão alterados (bordas)
        for(int countA = 0; countA < 3; countA++){
		for(int countB = 0; countB < getAltura(); countB++){
                        pixelsLocais[countA + countB*getAltura()] = (byte)getPixels()[countA + countB*getAltura()];
		}
	}
	for(int countA = 0; countA < 3; countA++){
		for(int countB = 0; countB < getLargura(); countB++){
                        pixelsLocais[countA + countB*getAltura()] = (byte)getPixels()[countA + countB*getAltura()];
		}
	}
        
        //Modifica os pixels que têm todos os "vizinhos"
	for(int countA = 3/2; countA < getAltura()-3/2; countA++){
		for(int countB = 3/2; countB < getLargura()-3/2; countB++){
                    valorPixel = 0;
			for(int countC = -3/2; countC <= 3/2; countC++){
				for(int countD = -3/2; countD <= 3/2; countD++){
					valorPixel += coeficiente[(countC + 1) + 3*(countD + 1)] * 
                                        (pixelsAux[(countA + countC) + (countD + countB) * getAltura()]);
				}
			}
				//Tratando caso o pixel seja menor ou ultrapasse o número de cores
				valorPixel /= 9;
				valorPixel = (valorPixel < 0) ? 0 : valorPixel;
				valorPixel = (valorPixel > getNumeroMaximoCor()) ? getNumeroMaximoCor() : valorPixel;
                                pixelsLocais[countA + countB*getAltura()] = (byte) valorPixel;
		}
	}        

            /*
            Frame frame = new JFrame();
            frame.setLayout(new FlowLayout());
            frame.add(new JLabel(new ImageIcon(filtroBlur)));
            frame.pack();
            frame.setVisible(true); */
        
        return filtroBlur;   
    }

    /*
     public static void main(String[] args) throws IOException {
        ImagemPgm pic = new ImagemPgm();
        
        
        pic.abrirArquivo();
        pic.pegaDados();
        pic.pegaInicioMensagem();
        pic.pegaPixels(); 
        //pic.decifraImagem();
       //pic.filtroNegativo();
       pic.filtroBlur();
        pic.fecharArquivo();
        
     }
}
 
    */
}

