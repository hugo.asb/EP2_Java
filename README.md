============================================================================================
		EP2 - Programa que aplica alguns filtros e esteganografia
============================================================================================

	Este programa em Java consegue ler arquivos dos tipos .ppm e .pgm, no qual o usuário
tem as seguintes opções de acordo com a extensão da imagem:

-->Imagens pgm: Estegonografia (Decifra mensagem escondida nos bytes da imagem);
		Filtro Negativo (Inverte as cores da imagem);
		Filtro Blur (Suavização);
		Filtro Blur (Nitidez);
-->Imagens ppm: Filtro Vermelho;
		Filtro Verde;
		Filtro Azul;
		Filtro Negativo;

-->Instruções: Como a aplicação conta com uma interface gráfica, o uso dentro do programa é muito
simples, porém, para abri-lo, siga os seguintes passos:
 - Entre na pasta do programa e abra a pasta dist/;
 - Na pasta dist/ abra o arquivo EsteganografiaEP2.jar;
 - Na tela inicial do programa, clique no botão 'Abrir Imagem' e escolha um arquivo;
 - Ao escolher a imagem outra janela irá aparecer com os fitros correspondentes para cada tipo de 
imagem e a imagem escolhida;
 - Escolha o filtro e aperte no botão 'Ativar';
 - Caso queira abrir outra imagem aperte no botão 'Abrir' e repita o processo. 